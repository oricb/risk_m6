package DAO;

import java.util.List;

import Model.Regio;

public interface IRegio extends IGenericDAO<Regio, String> {
	
	void saveOrUpdate(Regio r);
	//commit
	Regio get(int id);

	List<Regio> list();

	void delete(String id);

}
