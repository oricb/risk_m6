package DAO;

import Model.Joguina;

public class JoguinaDAO extends GenericDAO<Joguina, Integer>
{
	public JoguinaDAO() 
	{
		super.entityClass = Joguina.class;
	}
	
	
	public void cambiarNivelDeDiversion(Integer pk, int num) 
	{
		Joguina j = super.findByPK(pk);
		j.setNivellDiversio(num);
		super.update(j);
	}

}
