package DAO;

import java.util.ArrayList;

import Model.Continent;
import Model.Jugador;

public class JugadorDAO extends GenericDAO<Jugador, Integer>
{
	public JugadorDAO() 
	{
		super.entityClass = Jugador.class;
	}
	public Jugador getJugador(int id) {
		return super.findByPK(id);
	}
	public void LlistarContinentsJugador(int idJugador) {
		JugadorDAO j = new JugadorDAO();
		Jugador jj = j.getJugador(idJugador);
		
		ContinentDAO c = new ContinentDAO();
		ArrayList<Continent> cc = (ArrayList<Continent>) c.findAll();
		
		for(Continent c1 : cc) {
			if(c1.getJugador().getNom().equals(jj.getNom())) {
				System.out.println(c1.getNom());
			}
		}
	}

}