package DAO;

import java.util.ArrayList;
import java.util.List;

import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class RegioDAO extends GenericDAO<Regio, Integer>
{
	public RegioDAO() 
	{
		super.entityClass = Regio.class;
	}
	public Regio get(int id) {
		return super.findByPK(id);
	}
	//commit
	public void LlistarJugadorRegio(int id) {
		JugadorDAO j = new JugadorDAO();
		Jugador jj = j.getJugador(id);
		
		RegioDAO c = new RegioDAO();
		ArrayList<Regio> cc = (ArrayList<Regio>) c.findAll();
		
		for(Regio c1 : cc) {
			if(c1.getJugador().getNom().equals(jj.getNom())) {
				System.out.println(c1.getNom());
			}
		}
	}
	
	public String ComptarRegions(int CodiJugador) {
		int num = 0;
		List regions = super.findAll();
		for (int i = 0; i < regions.size(); i++) {
			Regio r = (Regio) regions.get(i);
			if (r.getJugador().getId() != CodiJugador) {
				num++;
			}
		}
		String s = "El jugador amb el codi " + CodiJugador + "té el valor " + num;
		return s;
	}
	

}
