package Main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Model.Etapa;
import Model.Tamagochi;

public class Main {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() 
	{
		if (sessionFactory == null) 
		{
			// exception handling omitted for brevityaa
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void main(String[] args) 
	{
		//Per començar a fer operacions a la BBDD necessitem instanciar una session
		session = getSessionFactory().openSession();
		
		//Abans de fer cap procediment, hem de començar una transacció:
		session.beginTransaction();
		Tamagochi tama1 = new Tamagochi();
		tama1.setNom("Simone");
		tama1.setDescripcio("Shaggy");
		tama1.setEtapa(Etapa.Baby);
		tama1.setViu(true);
		tama1.setFelicitat(100);
		tama1.setGana(33.00);
		//gana,datanaixement, felicitat
		
		Tamagochi tama2 = new Tamagochi();
		tama2.setNom("Marco");
		tama2.setDescripcio("Marco fiero el mejor");
		tama2.setEtapa(Etapa.Baby);
		tama2.setViu(true);
		//amb el session persist fem inserts a la BBDD de nous registres
		session.persist(tama1);
		session.persist(tama2);
		session.getTransaction().commit();
		session.getTransaction().begin();
		//amb el metode find() trobem un element per primary key
		Tamagochi tama1Id = session.find(Tamagochi.class,1);
		System.out.println(tama1Id);
		tama1Id.setNom("Updategotchi");
		session.merge(tama1Id);
		
		
		//no se si se tiene que guardar este cambio también en la BBDD
		
		/*Modificar a tamagotchi 1 el seu nom a “Updategotchi”.
		Consultar  tots els tamagotchis de la base de dades i mostrar les dades per la consola.
		Borrar l’objecte 1.
		Consultar tots els tamagotchis de la base de dades i mostrar les dades per la consola.*/

		
		List tamagochis = session.createQuery("from Tamagochi").getResultList(); 
		System.out.println(tamagochis);
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		session.remove(tama1);
		
		/*tamagochis = session.createQuery("from Tamagochi").getResultList(); 
		System.out.println(tamagochis);
		session.getTransaction().commit();*/
		
		List tamagochis2 = session.createQuery("from Tamagochi").getResultList(); 
		System.out.println(tamagochis2);
		session.getTransaction().commit();

		
		session.close();
	
	}



}
