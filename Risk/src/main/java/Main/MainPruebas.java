package Main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;

import DAO.ContinentDAO;
import DAO.JugadorDAO;
import DAO.RegioDAO;
import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class MainPruebas {
	
	public static void main(String[] args) 
	{
		RegioDAO reDAO = new RegioDAO();
		JugadorDAO juDAO=new JugadorDAO();
		ContinentDAO coDAO=new ContinentDAO();
		Regio r = new Regio();
		r.setNom("Catalunya");
		r.setNumTropes(33);
		Regio r12 = new Regio();
		r12.setNom("Baleares");
		r12.setNumTropes(33);
		Regio r2 = new Regio();
		r2.setNom("Kentucky");
		r2.setNumTropes(1);
		Jugador j = new Jugador();
		j.setNom("Ernesto");
		j.setOrdreTirada(1);
		j.setComptadorVictories(0);
		j.setNumRegions(1);
		Jugador j2 = new Jugador();
		j2.setNom("Paco");
		j2.setOrdreTirada(2);
		j2.setComptadorVictories(0);
		j2.setNumRegions(1);
		Continent c = new Continent();
		c.setNom("Europa");
		c.setBonusTropes(33);
		Continent c2 = new Continent();
		c2.setNom("America");
		c2.setBonusTropes(69);
		
		
		reDAO.save(r);
		reDAO.save(r12);
		reDAO.save(r2);
		juDAO.save(j);
		juDAO.save(j2);
		coDAO.save(c);
		coDAO.save(c2);
		
		Set<Regio> setr= new HashSet<Regio>();
		setr.add(r);
		setr.add(r12);
		j.setRegions(setr);
		Set<Regio> setr2= new HashSet<Regio>();
		setr2.add(r2);
		j2.setRegions(setr2);
		
		
		Set<Continent> setc= new HashSet<Continent>();
		setc.add(c);
		Set<Continent> setc2= new HashSet<Continent>();
		setc2.add(c2);
		c.setRegions(setr);
		c2.setRegions(setr2);
		
		j.setContinents(setc);
		j2.setContinents(setc2);
		Set<Jugador> setj= new HashSet<Jugador>();
		setj.add(j);
		Set<Jugador> setj2= new HashSet<Jugador>();
		setj2.add(j2);
		c.setJugador(j);
		c2.setJugador(j2);
		
		r12.setContinent(c);
		r.setContinent(c);
		r2.setContinent(c2);
		r.setVeins(setr2);
		r12.setVeins(setr2);
		r2.setVeins(setr);
		r.setJugador(j);
		r2.setJugador(j2);
		
		
		
		reDAO.update(r);
		reDAO.update(r12);
		reDAO.update(r2);
		juDAO.update(j);
		juDAO.update(j2);
		coDAO.update(c);
		coDAO.update(c2);
		
		juDAO.LlistarContinentsJugador(4);
		
		reDAO.LlistarJugadorRegio(4);
		
		
	}
}
