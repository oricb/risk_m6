package Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
//Anotacio per indicar el nom de la taula
@Table(name = "Joguines")

public class Joguina {
	@Id
	// @GeneratedValue indica a Hibernate com ha de generar la ID
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idJoguines")
	private int id;
	@Column(name = "nom", length = 100, nullable = false)
	private String nom;
	@Column(name = "descripcio", length = 50, nullable = false)
	private String descripcio;
	@Column(name = "nivellDiversio")
	private int nivellDiversio = 5;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_tamagochi")
	private Tamagochi tamagochi;
	
	
	public Tamagochi getTamagotchi() {
		return tamagochi;
	}
	public void setTamagotchi(Tamagochi tamagochi) {
		this.tamagochi = tamagochi;
	}
	
	@Override
	public String toString() {
		return "Joguina [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", nivellDiversio="
				+ nivellDiversio + "]";
	}
	public Joguina() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public int getNivellDiversio() {
		return nivellDiversio;
	}
	public void setNivellDiversio(int nivellDiversio) {
		this.nivellDiversio = nivellDiversio;
	}
	
	
}
